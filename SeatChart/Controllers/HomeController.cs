﻿using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using SeatChart.Models;
using System.Text;

namespace SeatChart.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SeatChart()
        {
            return View();
        }

        public ActionResult LayoutChart()
        {
            return View();
        }

        public ActionResult ManageCharts()
        {
            return View();
        }


        public ActionResult JsonToHtml()
        {

         string   jsonString = "[{\"seat_top\":\"184\",\"seat_left\":\"420\",\"seat_group\":\"\",\"seat_no\":\"\",\"seat_id\":\"0\"},{\"seat_top\":\"184\",\"seat_left\":\"453\",\"seat_group\":\"\",\"seat_no\":\"\",\"seat_id\":\"1\"},{\"seat_top\":\"184\",\"seat_left\":\"486\",\"seat_group\":\"\",\"seat_no\":\"\",\"seat_id\":\"2\"},{\"seat_top\":\"184\",\"seat_left\":\"519\",\"seat_group\":\"\",\"seat_no\":\"\",\"seat_id\":\"3\"}]";

         JavaScriptSerializer js = new JavaScriptSerializer();
         SeatModel[] seats = js.Deserialize<SeatModel[]>(jsonString);

         StringBuilder sb = new StringBuilder();

         foreach (var item in seats)
         {
             sb.Append("<div class=\"drag ui-selectee\" id=" + item.seat_id + " seat_group=\"" + item.seat_group + "\" seat_left=\"" + item.seat_left + "\" seat_top=\"" + item.seat_top + "\" style=\"  top:" + item.seat_top + "px;   left:" + item.seat_left + "px;  width: 26px; height: 26px; background-color: red;\">" + item.seat_no + " </div>");
         

         
         }
        

            return View();
        }
    }
}