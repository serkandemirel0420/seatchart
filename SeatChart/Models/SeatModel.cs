﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeatChart.Models
{
    public class SeatModel
    {
        public string seat_id { get; set; }
        public string seat_no { get; set; }
        public string seat_group { get; set; }

        public string seat_left { get; set; }
        public string seat_top { get; set; }


    }
}